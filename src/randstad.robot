*** Settings ***

Library  RequestsLibrary
Library  SeleniumLibrary
Library  String
Library  Collections

*** Variables ***

${browser}=  Firefox
${url}=  https://www.randstad.fr/
${sleep}=  2s
${cookies}=  //button[@id="onetrust-reject-all-handler"]
${job}=  boulanger
${region_list}=  //div[@class="selectric"]
${region_value}=  //li[contains(text(),"Bretagne")]
${search}=  //button[@onclick="link()"]
${offer}=  //a[@id="Job-1"]
${infos}=  //h3[contains(text(),"informations complémentaires")]/following-sibling::p
${assert_knowledge}=  niveau d'études
${assert_pay_value}=  salaire minimum
${assert_pay_type}=  type de salaire

*** Keyword ***

Connexion au site

	[Arguments]  ${url}  ${browser}

	# ouvrir du navigateur
	Open Browser  ${url}  ${browser}
	# mettre le navigateur en plein ecran
	Maximize Browser Window

Click si existant

	[Arguments]  ${element}

	# controler la presence de l'element
	${status}  ${value}=  Run Keyword And Ignore Error  Page Should Contain Element  ${element}
	# cliquer sur l'element si existant
	Run Keyword If  '${status}' == 'PASS'  Click Element  ${element}

Selection liste deroulante

	[Arguments]  ${list}  ${value}

	# ouvrir de la liste deroulante
	Click Element  ${list}
	# selectioner l'element dans la liste deroulante
	Click Element  ${value}

Recuperer une liste d_elements

	[Arguments]  ${locator}  ${separator}=\n

	# recuperer les elements
	${elements}=  Get Text  ${locator}
	# lister les elements
	${list_of_elements}=  Split String  ${elements}  ${separator}

	# renvoyer la liste d'elements
	[Return]  ${list_of_elements}

*** Test Cases ***

Test Statut Site

	[TAGS]  TNR  BOUTENBOUT  STATUTSITE

	[Documentation]  \nVerifier le statut en ligne du site

	# recuperer le code de statut du site
	${code}=  GET  ${url}
	# controler le code de statut comme etant 200
	Should Be Equal As Strings  OK  ${code.reason}

Test Infos Complementaires Emploi

	[TAGS]  TNR  BOUTENBOUT  INFOSJOBS

	[Documentation]  \nVerifier la presence du niveau d'etudes
	...  \nVerifier la presence du salaire minimum
	...  \nVerifier la presence du type de salaire

	# acceder au site
	Connexion au site  ${url}  ${browser}

	# gerer le temps de chargement du bandeau des cookies
	Sleep  ${sleep}
	# refuser les cookies
	Click si existant  ${cookies}
	# gerer le temps de suppression du bandeau des cookies
	Sleep  ${sleep}

	# entrer le metier "boulanger" dans le champ metier
	Input Text  id=id_What  ${job}
	# selectioner "Bretagne" dans le liste deroulante des region
	Selection liste deroulante  ${region_list}  ${region_value}
	# lancer la recherche
	Click Element  ${search}

	# acceder au premier metier issu de la recherche
	Click Element  ${offer}

	# recuperer les elements de la section informations complementaire
	${infos_data}=  Recuperer une liste d_elements  xpath=${infos}
	# recuperer l'element "niveau d'études"
	${knowledge}=  Get From List  ${infos_data}  0
	# recuperer l'element "salaire minimum"
	${pay_value}=  Get From List  ${infos_data}  1
	# recuperer l'element "type de salaire"
	${pay_type}=  Get From List  ${infos_data}  2
	# verifier l'element "niveau d'études"
	Should Contain  ${knowledge}  ${assert_knowledge}
	# verifier l'element "salaire minimum"
	Should Contain  ${pay_value}  ${assert_pay_value}
	# verifier l'element "type de salaire"
	Should Contain  ${pay_type}  ${assert_pay_type}

	# fermer le navigateur
	Close Browser
# Exercice Ausy

## Sujet de l'exercice

Réaliser sous 48 heures, dans l’outil que vous maitrisez le plus, mais idéalement robot framework, l’automatisation du scénario suivant :

- Ouverture de www.randstad.fr/

- Recherche depuis la page d’accueil d’un poste de « boulanger » en région « Bretagne »

- Cliquer sur « voir l’offre » du premier résultat affiché dans la liste

- Vérifier qu’il est indiqué dans les informations complémentaires le niveau d’étude minimum, le salaire et le type de salaire.

Résultat attendus si le candidat dispose d’un GitHub :

- hébergement de ses sources et résultats dessus

Sinon, dans le corps du mail :

- Copier-coller les sources de chaque fichier utilisé

- Lister les librairies et outils utilisés

- Fournir une capture d’écran des résultats obtenus, notamment de la vérification des informations complémentaires.

## Outils/Librairies utiliser

Tests effectués sous **Windows 11 Professionnel** avec **Python 3.10.0**

Script **robotframework** pour **Mozilla Firefox** écrit sous **notepad++**

**randstad.robot Settings :**

- **BuiltIn** (non-importée, **librarie robotframework par défaut**)

- **RequestsLibrary** (contrôle de statut **en ligne** du site pour débogage)

- **SeleniumLibrary** (xpath, click, etc...)

- **String** (récupération des données de la section **informations complémentaires** dans une liste, **librairie intégré robotframework**)

- **Collections** (récupération des éléments de la liste, **librairie intégré robotframework**)

## Installation

Installer **Python 3** en cochant **add to path**

Ajouter le repertoire **Scripts** de **Python 3** à la variable d'environnement **PATH**

Installer **robotframework** via la commande **pip install robotframework**

Installer **RequestsLibrary** via la commande **pip install robotframework-requests**

Installer **SeleniumLibrary** via la commande **pip install robotframework-seleniumlibrary**

## Utilisation via le launcher batch

Télecharger tout le git ou télecharger l'archive **robotframework-launcher.zip** et le script **randstad.robot** du dossier **src**

Dézipper l'archive **robotframework-launcher.zip** et utiliser le launcher batch réutilisable **robotframework-launcher.bat** (privilèges ou identifiants administrateur requis)

Indiquer le répertoire du script **randstad.robot** au launcher puis entrer **randstad** pour accéder à ce même script

Choisir les tests à lancer selon leurs **TAGS** (saisir **all** pour lancer tous les tests) puis indiquer ne pas ajouter de paramètre (saisir **none**)

## Utilisation via l'invité de commande

Télecharger tout le git ou uniquement le script **randstad.robot** du dossier **src**

Lancer un **invité de commande** en administrateur et se placer dans le répertoire du script **randstad.robot** via la commande **cd**

Lancer le test via la commande **robot randstad.robot**

## Sorties

Fichiers **geckodriver-1.log**, **log.html**, **output.xml** et **report.html**, générés aux cotés du script **randstad.robot** par l'exécution des tests, présents dans le dossier **src**

Rendu de la **console** :

![console](https://gitlab.com/AntoineTHEVENON/exercice-ausy/-/raw/main/screenshots/console_tests_pass.png)

Rendu du **rapport** :

![console](https://gitlab.com/AntoineTHEVENON/exercice-ausy/-/raw/main/screenshots/report_tests_pass.png)

Rendu des **logs** :

![console](https://gitlab.com/AntoineTHEVENON/exercice-ausy/-/raw/main/screenshots/logs_tests_pass.png)

Rendu des **assertions** sur les **informations complémentaires** :

![console](https://gitlab.com/AntoineTHEVENON/exercice-ausy/-/raw/main/screenshots/assert_infos_complementaires.png)